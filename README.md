# VueJs-todos
Un simple projet pour comprendre le fonctionement de Vue js , c'est une application qui permet de gérer les tâches (Todos)
vous pouvez ajouter , supprimer ou modifier une tâche par le double-clic sur le nom de la tâche dans la liste
vous pouvez aussi filtrer par les tâches faites , à faire ou afficher toutes les tâches sans aucun filtre

pour utiliser l'application , il suffit d'executer les deux commandes suivantes:

1) npm install
2) npm run serve

pré-requis:
nodejs installé



Je vous remercie :)




